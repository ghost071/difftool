#!/usr/bin/env node
import program from 'commander';
import compare from '..';
import { readFileSync } from 'fs'

const ver = json.Parse(readFileSync("../../package.json")).version

program.version(ver)
  .description('Compares two configuration files and shows a difference')
  .option('-f, --format [type]', 'Output format')
  .arguments('<firstConfig> <secondConfig>')
  .action((firstConfig, secondConfig) => {
    console.log(compare(firstConfig, secondConfig, program.format));
  });

program.parse(process.argv);
